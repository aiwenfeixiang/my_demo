# -*- coding: utf-8 -*-
"""
    Author : YangKe
    Time : 5/10/22 8:41 AM
"""

"""注册用户"""


def register():
    """
    注册用户
    :return:
    """
    while 1:
        content = input("是否开始注册(Y/N):")
        if content.upper() == "N":
            print("退出程序")
            break
        elif content.upper() == "Y":
            userName = input("用户名：").strip()
            passWord = input("密码：").strip()
        with open("../config/user_msg", encoding="utf-8", mode="a") as f:
            f.write(f"{userName}|{passWord}\n")


if __name__ == '__main__':
    register()
